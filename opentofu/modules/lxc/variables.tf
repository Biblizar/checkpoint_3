variable "venv_endpoint" {
  description = "Le endpoint de notre environnement virtuel"
  type        = string
  default     = ""
}

variable "venv_api_token" {
  description = "Le token de l'api proxmox"
  type        = string
  default     = ""
}

variable "ssh_username" {
  description = "Le username de notre connexion ssh"
  type        = string
  default     = ""
}

variable "tmp_dir" {
  description = "Le dossier temporaire"
  type        = string
  default     = ""
}

variable "ipv4_addrss" {
  description = "L'ip du conteneur'"
  type        = string
  default     = ""
}

variable "ct_network_bridge" {
  description = "le bridge du network"
  type        = string
  default     = ""
}
variable "ct_network_name" {
  description = "le nom du network"
  type        = string
  default     = ""
}

variable "gateway" {
  description = "Passerelle"
  type        = string
  default     = ""
}

variable "dns_servers" {
  description = "Les serveurs dns utilisés"
  type        = list(string)
  default     = []
}

variable "dns_domain" {
  description = "Le nom de domaine utilisé"
  type        = string
  default     = ""
}

variable "os_type" {
  description = "Le type d'os utilisé"
  type        = string
  default     = ""
}

variable "ct_memory_dedicated" {
  description = "La memoire utilisé par le container"
  type        = string
  default     = ""
}

variable "ct_disk_size" {
  description = "La taille du disk du container"
  type        = string
  default     = ""
}

variable "ct_datastore_storage_location" {
  description = "L'emplacement de stockage des données"
  type        = string
  default     = ""
}

variable "ct_id" {
  description = "L'id du container"
  type        = string
  default     = ""
}

variable "node_name" {
  description = "Le nom du node du container"
  type        = string
  default     = ""
}

variable "ct_template_file" {
  description = "Le template de container utilisé"
  type        = string
  default     = ""
}