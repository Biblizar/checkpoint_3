<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_proxmox"></a> [proxmox](#requirement\_proxmox) | >= 0.38.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_proxmox"></a> [proxmox](#provider\_proxmox) | >= 0.38.1 |
| <a name="provider_random"></a> [random](#provider\_random) | n/a |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |
| <a name="provider_tls"></a> [tls](#provider\_tls) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [proxmox_virtual_environment_container.debian_container](https://registry.terraform.io/providers/bpg/proxmox/latest/docs/resources/virtual_environment_container) | resource |
| [random_password.debian_container_password](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password) | resource |
| [terraform_data.ansible_inventory](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/resources/data) | resource |
| [tls_private_key.debian_container_key](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ipv4_addrss"></a> [ipv4\_addrss](#input\_ipv4\_addrss) | L'ip du conteneur' | `string` | `""` | no |
| <a name="input_ssh_username"></a> [ssh\_username](#input\_ssh\_username) | Le username de notre connexion ssh | `string` | `""` | no |
| <a name="input_tmp_dir"></a> [tmp\_dir](#input\_tmp\_dir) | Le dossier temporaire | `string` | `""` | no |
| <a name="input_venv_api_token"></a> [venv\_api\_token](#input\_venv\_api\_token) | Le token de l'api proxmox | `string` | `""` | no |
| <a name="input_venv_endpoint"></a> [venv\_endpoint](#input\_venv\_endpoint) | Le endpoint de notre environnement virtuel | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_debian_container_password"></a> [debian\_container\_password](#output\_debian\_container\_password) | n/a |
<!-- END_TF_DOCS -->