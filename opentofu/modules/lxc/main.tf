resource "random_password" "debian_container_password" {
  length           = 16
  override_special = "_%@"
  special          = true
}
output "debian_container_password" {
  value     = random_password.debian_container_password.result
  sensitive = true
}

resource "tls_private_key" "debian_container_key" {
  algorithm = "ED25519"
  rsa_bits  = 4096
}

resource "proxmox_virtual_environment_container" "debian_container" {
  description   = "Our Debian Container with OpenTofu"
  node_name     = var.node_name
  start_on_boot = true
  tags          = ["debian12", "checkpoint_3", "ansible"]
  unprivileged  = true
  vm_id         = var.ct_id

  cpu {
    architecture = "amd64"
    cores        = 2
  }
  disk {
    datastore_id = var.ct_datastore_storage_location
    size         = var.ct_disk_size
  }
  memory {
    dedicated = var.ct_memory_dedicated
    swap      = 0
  }
  operating_system {
    template_file_id = var.ct_template_file
    type             = var.os_type
  }

  initialization {
    dns {
      domain  = var.dns_domain
      servers = var.dns_servers
    }
    ip_config {
      ipv4 {
        address = var.ipv4_addrss
        gateway = var.gateway
      }
    }

    user_account {
      keys = [
        trimspace(tls_private_key.debian_container_key.public_key_openssh)
      ]
      password = random_password.debian_container_password.result
    }
  }

  network_interface {
    name   = var.ct_network_name
    bridge = var.ct_network_bridge
  }

  features {
    nesting = true
    fuse    = false
  }
}

resource "terraform_data" "ansible_inventory" {
  depends_on = [tls_private_key.debian_container_key]

  provisioner "local-exec" {
    command = "bash ${path.module}/update_ansible_inventory.sh ${var.ipv4_addrss} '${base64encode(tls_private_key.debian_container_key.private_key_pem)}'"
  }
}
