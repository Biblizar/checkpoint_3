#!/bin/bash

touch ../ansible/host.ini
echo "[all]" > ../ansible/hosts.ini
echo "$1 ansible_user=root ansible_ssh_private_key_file=./private_key.pem >> ../ansible/hosts.ini"
echo "$2" | base64 --decode > ../ansible/private_key.pem
chmod 600 ../ansible/private_key.pem